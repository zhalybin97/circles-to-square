using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_controller : MonoBehaviour
{
    [SerializeField] private GameObject restartButton;
    [SerializeField] private GameObject scoreLable;

    public void CounterReset()
    {
        scoreLable.GetComponent<Counter>().Clear();
    }

    public void RestartButtonShow()
    {
        restartButton.SetActive(true);
    }
}
