using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    private bool isReadyToDrag = false;
    private Vector3 delta;

    private void OnMouseDown()
    {
        delta = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
        isReadyToDrag = true;
    }

    private void OnMouseDrag()
    {
        if(isReadyToDrag)
        {
            Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition) + delta;
            newPos.z = 0;
            transform.position = newPos;
        }
            
    }

    private void OnMouseUp()
    {
        isReadyToDrag = false; 
    }

    public void ResetPosition()
    {
        transform.position = new Vector3(0,0.6f,0);
    }
}
