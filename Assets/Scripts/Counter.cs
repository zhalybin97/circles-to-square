using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Counter : MonoBehaviour
{
    private int score = 0;
    [SerializeField] private GameObject restartButton;
    public void AddOne()
    {
        score++;
        GetComponent<TextMeshProUGUI>().text = "Score: " + score;

        if(score == 5)
            restartButton.SetActive(true);
    }

    public void Clear()
    {
        score = 0;
        GetComponent<TextMeshProUGUI>().text = "Score: " + score;
    }
}
