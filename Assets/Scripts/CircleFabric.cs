using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleFabric : MonoBehaviour
{
    [SerializeField] private Color[] colors;
    [SerializeField] private Vector4 spawnArea;

    [SerializeField] private GameObject circlePrefab;
    private GameObject[] circles = new GameObject[5];

    private void Start()
    {
        Spawn();
    }

    private void Spawn()
    {
        for (int i = 0; i < circles.Length; i++)
        {
            circles[i] = Instantiate<GameObject>(circlePrefab, new Vector3(Random.Range(spawnArea.x, spawnArea.y), Random.Range(spawnArea.z, spawnArea.w), 0), new Quaternion(0, 0, 0, 1));
            circles[i].GetComponent<SpriteRenderer>().color = colors[Random.Range(0, colors.Length - 1)];
        }
    }

    public void Respawn()
    {
        FindObjectOfType<Counter>().Clear();
        foreach(GameObject t in circles)
        {
            t.transform.localPosition = new Vector3(Random.Range(spawnArea.x, spawnArea.y), Random.Range(spawnArea.z, spawnArea.w), 0);
            t.GetComponent<SpriteRenderer>().color = colors[Random.Range(0, colors.Length - 1)];
            t.SetActive(true);
        }
    }
}
