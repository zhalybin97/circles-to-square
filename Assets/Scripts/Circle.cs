using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : MonoBehaviour
{
    public delegate void OnCollisionEnter();
    public event OnCollisionEnter onCollisionEnter;

    private void Start()
    {
        onCollisionEnter += FindObjectOfType<Counter>().AddOne;
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag != "Player")
            return;

        this.gameObject.SetActive(false);
        onCollisionEnter.Invoke();
    }
}
