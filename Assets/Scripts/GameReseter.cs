using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameReseter : MonoBehaviour
{
    [SerializeField] GameObject square;
    [SerializeField] GameObject circleFabric;
    [SerializeField] GameObject UI_Controller;

    public void Restart()
    {
        square.GetComponent<Controller>().ResetPosition();
        circleFabric.GetComponent<CircleFabric>().Respawn();
        UI_Controller.GetComponent<UI_controller>().CounterReset();
    }
}
